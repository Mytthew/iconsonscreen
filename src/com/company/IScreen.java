package com.company;

public interface IScreen {
	/**
	 * Dodaje nową ikonę do ekranu
	 * @param x współrzędna x-owa pozycji ikony
	 * @param y współrzedna y-owa pozycji ikony
	 * @param name nazwa ikony
	 */

	void addIcon(int x, int y, String name);

	/**
	 * Metoda zwraca informacje o ikonach na ekranie
	 * @return jeden obiekt typu String, w którym nazwa
	 * każdej ikony jest w osobnej linijce
	 */
	String getInfo();
}
