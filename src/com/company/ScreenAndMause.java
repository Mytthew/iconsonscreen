package com.company;


public class ScreenAndMause {

    public static void main(String[] args) {
		IScreen screen = new Screen();
		screen.addIcon(0, 0, "Internet browser");
		screen.addIcon(1, 0, "Java IDE");
		screen.addIcon(2, 0, "Music player" );

		IMouse mouse = ((Screen)screen).new Mouse();
		System.out.println(mouse.getIconName());
		mouse.slideCursor(1, 0);
		System.out.println(mouse.getIconName());
		mouse.slideCursor(0, 1);
		System.out.println(mouse.getIconName());
    }
}
