package com.company;

public interface IMouse {
	/**
	 * Przesuń kursos myszki
	 * @param dx przesunięcie myszki w kierunku osi OX
	 * @param dy przesunięcie myszki w kierunku osi OY
	 */
	void slideCursor(int dx, int dy);

	/**
	 * Odczytaj na co wskazuje mysz
	 * @return nazwa ikony na którą wskazuje kursor myszki,
	 * albo null jeżeli mysz nie wskazuje na żadną ikonę.
	 */
	String getIconName();
}
