package com.company;


import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Screen implements IScreen {
	private List<Icon> icons = new ArrayList<>();


	public class Icon {
		int x;
		int y;
		String name;

		private void setX(int x) {
			this.x = x;
		}

		private void setY(int y) {
			this.y = y;
		}

		private void setName(String name) {
			this.name = name;
		}

		public int getX() {
			return x;
		}

		public int getY() {
			return y;
		}

		private String getName() {
			return name;
		}
	}

	class Mouse implements IMouse {
		private int x;
		private int y;

		private int getX() {
			return x;
		}

		private int getY() {
			return y;
		}

		public void slideCursor(int dx, int dy) {
			x = getX() + dx;
			y = getY() + dy;
		}

		public String getIconName() {
			for (int i = 0; i < icons.size(); i++) {
				if (x == icons.get(i).getX() && y == icons.get(i).getY()) {
					return icons.get(i).getName();
				}
			}
			return null;
		}
	}

	@Override
	public void addIcon(int x, int y, String name) {
		Icon icon = new Icon();
		icons.add(icon);
		icon.setX(x);
		icon.setY(y);
		icon.setName(name);
	}

	public String getInfo() {
		StringBuilder result = new StringBuilder();
		for (int i = 0; i < icons.size(); i++) {
			result.append(icons.get(i).getName()).append("\n");
		}
		return result.toString();
	}
}
